%global OpenStackVersion victoria
Summary: OpenStack from the CentOS Cloud SIG repo configs
Name: centos-release-openstack-%{OpenStackVersion}
Epoch: 666
Version: 1
Release: 3%{?dist}
License: GPL
URL: http://wiki.centos.org/SpecialInterestGroup/Cloud
Source0: CentOS-OpenStack.repo
Source1: RPM-GPG-KEY-CentOS-SIG-Cloud

BuildArch: noarch

Requires: centos-release
Requires: centos-release-advanced-virtualization
Requires: centos-release-rabbitmq-38
Requires: centos-release-ceph-nautilus
Requires: centos-release-nfv-openvswitch
Conflicts: centos-release-openstack

%description
yum Configs and basic docs for OpenStack as delivered via the CentOS Cloud SIG.

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/CentOS-OpenStack-%{OpenStackVersion}.repo
sed -i -e "s/OPENSTACK_VERSION/%{OpenStackVersion}/g" %{buildroot}%{_sysconfdir}/yum.repos.d/CentOS-OpenStack-%{OpenStackVersion}.repo
install -p -d %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/pki/rpm-gpg

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/*
%{_sysconfdir}/pki/rpm-gpg

%changelog
* Tue Feb 08 2022 - Alex Iribarren <Alex.Iribarren@cern.ch> - %{OpenStackVersion}-1-3
- CERN rebuild, pointing repos to linuxsoft.cern.ch

* Mon Jan 10 2022 Joel Capitao <jcapitao@redhat.com> - %{OpenStackVersion}-1-3
- Remove the logic to identify CL8 or CS8

* Fri Mar 12 2021 - Alex Iribarren <Alex.Iribarren@cern.ch> - %{OpenStackVersion}-1-2
- CERN rebuild, pointing repos to linuxsoft.cern.ch

* Mon Mar 08 2021 Alfredo Moralejo <amoralej@redhat.com> - %{OpenStackVersion}-1-2
- Add support for CentOS Stream 8 repos.

* Fri Nov 13 2020 - Alex Iribarren <Alex.Iribarren@cern.ch> - %{OpenStackVersion}-1-1
- CERN rebuild, pointing repos to linuxsoft.cern.ch

* Tue Nov 3 2020 Yatin Karel <ykarel@redhat.com> - %{OpenStackVersion}-1-1
- Victoria Release
